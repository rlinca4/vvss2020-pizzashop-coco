package pizzashop.service;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import pizzashop.exception.ServerException;
import pizzashop.exception.ValidationException;
import pizzashop.model.enums.PaymentType;
import pizzashop.repository.PaymentRepository;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PaymentServiceTest {

    private static PaymentService paymentService;

    @BeforeAll
    static void setUp() throws IOException {
        PaymentRepository payRepo = new PaymentRepository();
        paymentService = new PaymentService(payRepo);
    }

    @RepeatedTest(value = 3, name = RepeatedTest.LONG_DISPLAY_NAME)
    void addPayment_addNewRecord_whenTableIsNatural_ecp1() throws ServerException, ValidationException, IOException {
        // Setup
        int initialSize = paymentService.getPayments().size();

        // Act
        paymentService.addPayment(2, PaymentType.CARD, 10);

        // Assert
        assertEquals(paymentService.getPayments().size(), initialSize + 1);
    }


    @DisplayName("Payment Test: rational amount")
    @ParameterizedTest
    @ValueSource(doubles = { 255.6, 145.15 })
    void addPayment_addNewRecord_whenAmountIsRational_ecp1(double amount) throws ServerException, ValidationException, IOException {
        int initialSize = paymentService.getPayments().size();
        paymentService.addPayment(3, PaymentType.CARD, amount);

        assertEquals(paymentService.getPayments().size(), initialSize + 1);
    }


    @Test
    @Tag("development")
    void addPayment_throwsException_whenTableNotInInterval_ecp3() throws IOException {
        int initialSize = paymentService.getPayments().size();

        assertThrows(ValidationException.class, () -> {
            paymentService.addPayment(9, PaymentType.CARD, 66.5);
            assertEquals(paymentService.getPayments().size(), initialSize);
        });
    }

    @Test
    @Tag("development")
    void addPayment_throwsException_whenTableNotInIntervalAndNegative_ecp4() throws IOException {
        int initialSize = paymentService.getPayments().size();

        assertThrows(ValidationException.class, () -> {
            paymentService.addPayment(-5, PaymentType.CASH, 20.5);
            assertEquals(paymentService.getPayments().size(), initialSize);
        });
    }

    @Test
    @DisplayName("Payment Test: negative amount")
    void addPayment_throwsException_negativeAmount_ecp5() throws IOException {
        int initialSize = paymentService.getPayments().size();

        assertThrows(ValidationException.class, () -> {
            paymentService.addPayment(5, PaymentType.CARD, -24.5);
            assertEquals(paymentService.getPayments().size(), initialSize);
        });
    }

    @Nested
    @DisplayName("BVATests")
    public class BVA {
            @DisplayName("Table is lower than first limit")
            @ParameterizedTest
            @ValueSource(ints = { -1})
            void addPayment_throwsException_whenTableBelowLowerLimit(int table) {
                assertThrows(ValidationException.class, () -> {
                    paymentService.addPayment(table, PaymentType.CARD, 66.5);
                });
            }

            @ParameterizedTest
            @ValueSource(ints = { 0, 1})
            @DisplayName("Table is higher or equal than first limit")
            void addPayment_addNewRecord_whenTableAboveLowerLimit(int table) throws ServerException, ValidationException, IOException {
                int initialSize = paymentService.getPayments().size();
                paymentService.addPayment(table, PaymentType.CARD, 66.5);

                assertEquals(paymentService.getPayments().size(), initialSize + 1);
            }

            @DisplayName("Table is higher than second limit")
            @ParameterizedTest
            @ValueSource(ints = { 8})
            void addPayment_throwsException_whenTableAboveUpperLimit(int table) {
                assertThrows(ValidationException.class, () -> {
                    paymentService.addPayment(table, PaymentType.CARD, 66.5);
                });
            }

            @ParameterizedTest
            @ValueSource(ints = { 6, 7})
            @DisplayName("Table is lower or equal than second limit")
            void addPayment_addNewRecord_whenTableBelowUpperLimit(int table) throws ServerException, ValidationException, IOException {
                int initialSize = paymentService.getPayments().size();
                paymentService.addPayment(table, PaymentType.CARD, 66.5);

                assertEquals(paymentService.getPayments().size(), initialSize + 1);
            }
        }

    @AfterEach
    void tearDown() {}


}