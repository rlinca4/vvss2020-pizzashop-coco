package pizzashop.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pizzashop.exception.ServerException;
import pizzashop.exception.ValidationException;
import pizzashop.model.Payment;
import pizzashop.model.enums.PaymentType;
import pizzashop.repository.PaymentRepository;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class ServiceIntegrationTest_Step2_MockedPayment {

    @Mock
    private Payment payment;

    private PaymentRepository paymentRepository;

    private PaymentService paymentService;

    private static String testPaymentsFile = "data/test_payments.txt";
    private static String testPaymentsFileCompletePath = "src/main/resources/" + testPaymentsFile;

    @BeforeEach
    void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        // Create test file for payments
        File file = new File(testPaymentsFileCompletePath);
        file.createNewFile();

        paymentRepository = new PaymentRepository(testPaymentsFile);
        paymentService = new PaymentService(paymentRepository);
    }

    @Test
    void addPayment_newPaymentAdded_mockedAndRealPayment() throws IOException, ServerException, ValidationException {
        // Setup
        paymentRepository.add(payment);

        // Assert
        paymentService.addPayment(2, PaymentType.CASH, 10d);
        int totalSize = paymentService.getPayments().size();
        assertEquals(totalSize, 2);

        // Mockito verify
        Mockito.verify(payment, Mockito.times(0)).getAmount();
    }

    @Test
    void getTotalAmount_returnTotal_forMockedPayments() throws IOException {
        // Setup
        when(payment.getAmount()).thenReturn(50d);
        when(payment.getType()).thenReturn(PaymentType.CARD);
        paymentRepository.add(payment);
        paymentRepository.add(payment);

        // Assert
        double totalPaymentsForCard = paymentService.getTotalAmount(PaymentType.CARD);
        assertEquals(totalPaymentsForCard, 100d);

        // Mockito verify
        Mockito.verify(payment, Mockito.times(2)).getAmount();
    }


    @AfterEach
    void tearDown() throws IOException {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        File file = new File(classloader.getResource(testPaymentsFile).getFile());
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write("");
        }
    }
}