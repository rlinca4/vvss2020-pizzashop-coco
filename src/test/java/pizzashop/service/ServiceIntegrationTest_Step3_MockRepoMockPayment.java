package pizzashop.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import pizzashop.exception.ServerException;
import pizzashop.exception.ValidationException;
import pizzashop.model.Payment;
import pizzashop.model.enums.PaymentType;
import pizzashop.repository.PaymentRepository;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("Duplicates")
class ServiceIntegrationTest_Step3_MockRepoMockPayment {

    private Payment payment;

    private PaymentRepository paymentRepository;

    private PaymentService paymentService;

    private static String testPaymentsFile = "data/test_payments.txt";
    private static String testPaymentsFileCompletePath = "src/main/resources/" + testPaymentsFile;

    @BeforeEach
    void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        // Create test file for payments
        File file = new File(testPaymentsFileCompletePath);
        file.createNewFile();

        paymentRepository = new PaymentRepository(testPaymentsFile);
        paymentService = new PaymentService(paymentRepository);
        payment = new Payment(2, PaymentType.CASH, 10d);
    }


    @Test
    void addPayment_worksAsExpected_whenPaymentIsValid() throws ServerException, ValidationException, IOException {
        // Setup
        paymentRepository.add(payment);

        // Assert
        paymentService.addPayment(2, PaymentType.CASH, 10d);
        int totalSize = paymentService.getPayments().size();
        assertEquals(2, totalSize);

    }

    @Test
    void getTotalAmount_returnsTheExpectedValue_whenPaymentsListIsNotEmpty() throws IOException {
        // Setup
        paymentRepository.add(payment);
        paymentRepository.add(payment);

        // Assert
        double totalPaymentsForCard = paymentService.getTotalAmount(PaymentType.CASH);
        assertEquals(20d, totalPaymentsForCard);
    }

    @AfterEach
    void tearDown() throws IOException {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        File file = new File(classloader.getResource(testPaymentsFile).getFile());
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write("");
        }
    }
}