package pizzashop.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.enums.PaymentType;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class PaymentTest {

    private Payment payment;

    @BeforeEach
    void init() {
        this.payment = new Payment();
    }

    @Test
    void setTableNumber_setsCorrectValue_whenSetterIsCalled(){
        // Prepare
        final int tableNumber = 1;
        final int defaultTableNumber = this.payment.getTableNumber();
        assertNotEquals(tableNumber, defaultTableNumber);

        // Assert
        this.payment.setTableNumber(tableNumber);

        // Verify mockito
        assertEquals(tableNumber, this.payment.getTableNumber());
    }

    @Test
    void setPaymentType_setsCorrectValue_whenSetterIsCalled(){
        // Prepare
        final PaymentType paymentType = PaymentType.CARD;
        final PaymentType defaultPaymentType = this.payment.getType();
        assertNotEquals(paymentType, defaultPaymentType);

        // Assert
        this.payment.setType(paymentType);

        // Verify mockito
        assertEquals(paymentType, this.payment.getType());
    }


}
