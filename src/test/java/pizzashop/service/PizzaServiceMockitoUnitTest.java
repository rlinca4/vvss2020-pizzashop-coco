package pizzashop.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pizzashop.exception.ServerException;
import pizzashop.exception.ValidationException;
import pizzashop.model.Payment;
import pizzashop.model.enums.PaymentType;
import pizzashop.repository.IRepository;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class PizzaServiceMockitoUnitTest {

    @Mock
    private IRepository<Payment, String> repository;

    @InjectMocks
    private PaymentService paymentService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void addPayment_paymentIsNotAdded_whenPaymentIsValid() throws IOException, ServerException, ValidationException {
        // Setup
        Payment newPayment = new Payment(2, PaymentType.CARD, 25d);
        Payment initialPayment = new Payment(2, PaymentType.CARD, 50d);

        when(repository.getAll()).thenReturn(Arrays.asList(initialPayment, newPayment));

        // Assert
        paymentService.addPayment(5, PaymentType.CARD, 10d);

        assertEquals(paymentService.getPayments().size(), 2);

        // Verify mockito
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }

    @Test
    void getTotalAmount_returnTotal() throws IOException {
        // Setup
        Payment initialPayment = new Payment(2, PaymentType.CARD, 50d);
        Payment newPayment = new Payment(3, PaymentType.CARD, 25d);

        when(repository.getAll()).thenReturn(Arrays.asList(initialPayment, newPayment));

        // Assert
        double totalPaymentsForCard = paymentService.getTotalAmount(PaymentType.CARD);
        assertEquals(totalPaymentsForCard, 75d);

        // Verify mockito
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }

    @AfterEach
    void tearDown() {
    }
}