package pizzashop.service;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;
import pizzashop.exception.ServerException;
import pizzashop.exception.ValidationException;
import pizzashop.model.Payment;
import pizzashop.model.enums.PaymentType;
import pizzashop.repository.PaymentRepository;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


class PaymentServiceTestWB {
    private static PaymentService paymentService;
    private static PaymentRepository payRepo;
    private static String testPaymentsFile = "data/test_payments.txt";
    private static String testPaymentsFileCompletePath = "src/main/resources/" + testPaymentsFile;

    @BeforeAll
    static void setUp() throws IOException {

        File file = new File(testPaymentsFileCompletePath);
        if(file.createNewFile()) {
            System.out.println("Test payments file has been created.");
        }

        payRepo = new PaymentRepository(testPaymentsFile);
        paymentService = new PaymentService(payRepo);
    }

    @Test
    @Tag("Development_wbTest")
    void getTotalAmount_returnTotal_whenPaymentListIsEmpty() throws IOException {

        // Act
        double total = paymentService.getTotalAmount(PaymentType.CARD);

        // Assert
        assertEquals(total, 0);
    }

    @ParameterizedTest
    @EnumSource(value = PaymentType.class, names = { "CASH" })
    void getTotalAmount_returnTotal_whenPaymentTypeIsNotInThePaymentsList_oneLoop(PaymentType type)
            throws ServerException, ValidationException, IOException {

        //Setup
        // Add one record for test
        paymentService.addPayment(2, type, 50d);

        // Act
        double total = paymentService.getTotalAmount(PaymentType.CARD);

        // Assert
        assertEquals(total, 0);

    }


    @Tag("Development_wbTest_twoLoops_existingPaymentType")
    @ParameterizedTest
    @EnumSource(value = PaymentType.class, names = { "CASH" })
    void getTotalAmount_returnTotal_whenPaymentTypeIsInThePaymentsList_twoLoops(PaymentType type)
            throws ServerException, ValidationException, IOException {

        //Setup
        // Add two records for test
        paymentService.addPayment(2, type, 50d);
        paymentService.addPayment(2, type, 45d);

        // Act
        double total = paymentService.getTotalAmount(PaymentType.CASH);

        // Assert
        assertEquals(total, 95d);

    }

    @DisplayName("GetTotalAmount Test: n loops")
    @ParameterizedTest
    @ValueSource(ints = { 8 })
    void getTotalAmount_returnTotal_whenPaymentTypeIsNotInThePaymentsList_nLoops(int n)
            throws IOException, ServerException, ValidationException
    {
        //Setup
        double constantAmount = 10d;
        // Add n records for test
        for (int i=0; i<n; i++) {
            paymentService.addPayment(3, PaymentType.CARD, constantAmount);
        }

        // Act
        double totalForCash = paymentService.getTotalAmount(PaymentType.CASH);

        // Assert
        assertEquals(totalForCash, 0);
    }

    @Test
    @Tag("Development_wbTest")
    void getTotalAmount_returnTotal_whenPaymentListIsNull() throws IOException {
        //Setup
        this.payRepo.setPaymentList(null);

        // Act
        double total = paymentService.getTotalAmount(PaymentType.CARD);

        // Assert
        assertEquals(total, 0);

        //Cleanup
        this.payRepo.setPaymentList(new ArrayList<>());
    }

    @ParameterizedTest
    @EnumSource(value = PaymentType.class, names = { "CASH" })
    void getTotalAmount_returnTotal_whenPaymentTypeIsInThePaymentsList_oneLoop(PaymentType type)
            throws ServerException, ValidationException, IOException {
        // Add one record for test
        paymentService.addPayment(1, type, 50d);

        // Act
        double total = paymentService.getTotalAmount(PaymentType.CASH);

        // Assert
        assertEquals(total, 50d);
    }

    @DisplayName("GetTotalAmount Test: n-1 loops")
    @ParameterizedTest
    @ValueSource(ints = { 8 })
    void getTotalAmount_returnTotal_whenPaymentTypeIsNotInThePaymentsList_LessThannLoops(int n)
            throws ServerException, ValidationException, IOException {
        //Setup
        double constantAmount = 10d;
        // Add n-1 records for test
        for (int i=0; i<n-1; i++) {
            paymentService.addPayment(3, PaymentType.CARD, constantAmount);
        }

        // Act
        double totalForCash = paymentService.getTotalAmount(PaymentType.CASH);

        // Assert
        assertEquals(totalForCash, 0);
    }

    @AfterEach
    void tearDown() throws IOException {
        paymentService.getPayments().clear();

        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        File file = new File(classloader.getResource(testPaymentsFile).getFile());
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write("");
        }
    }
}