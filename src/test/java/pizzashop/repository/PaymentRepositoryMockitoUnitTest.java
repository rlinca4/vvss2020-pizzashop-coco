package pizzashop.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pizzashop.model.Payment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class PaymentRepositoryMockitoUnitTest {

    @Mock
    private Payment payment;

    private PaymentRepository paymentRepository;

    private static String testPaymentsFile = "data/test_payments.txt";
    private static String testPaymentsFileCompletePath = "src/main/resources/" + testPaymentsFile;

    @BeforeEach
    void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        // Create test file for payments
        File file = new File(testPaymentsFileCompletePath);
        file.createNewFile();

        paymentRepository = new PaymentRepository(testPaymentsFile);
        paymentRepository.add(payment);
    }

    @Test
    void addMockPayment_testGetAllSize() throws IOException {
        // Setup
        paymentRepository.add(payment);

        // Assert
        assertEquals(paymentRepository.getAll().size(), 2);
    }

    @Test
    void removeMockPayment_testGetAllSize() throws IOException {
        // Setup
        paymentRepository.remove(payment);

        // Assert
        assertEquals(paymentRepository.getAll().size(), 0);
    }

    @AfterEach
    void tearDown() throws IOException {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        File file = new File(classloader.getResource(testPaymentsFile).getFile());
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write("");
        }
    }
}