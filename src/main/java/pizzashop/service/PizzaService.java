package pizzashop.service;

import pizzashop.exception.ServerException;
import pizzashop.model.MenuItem;
import pizzashop.model.Order;
import pizzashop.repository.MenuRepository;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class PizzaService {

    private MenuRepository menuRepo;

    public PizzaService(MenuRepository menuRepo) {
        this.menuRepo = menuRepo;
    }

    public List<MenuItem> getMenuData() throws ServerException {
        try {
            return menuRepo.getAll();
        } catch (IOException e) {
            throw new ServerException(e.getMessage());
        }
    }

    public List<Order> getPossibleOrders() throws ServerException {
        try {
            return menuRepo.getAll()
                    .stream()
                    .map(item -> new Order(item, 0))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new ServerException(e.getMessage());
        }
    }

    public List<String> getActualOrdersToString(List<Order> menuData) {
        return menuData.stream()
                .filter(x -> x.getQuantity() > 0)
                .map(order -> order.getQuantity() + " " + order.getMenuItem().getName())
                .collect(Collectors.toList());
    }
}
