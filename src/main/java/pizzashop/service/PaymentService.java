package pizzashop.service;

import pizzashop.exception.ServerException;
import pizzashop.exception.ValidationException;
import pizzashop.model.Order;
import pizzashop.model.Payment;
import pizzashop.model.enums.PaymentType;
import pizzashop.repository.IRepository;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class PaymentService {
    private IRepository<Payment, String> payRepo;

    public PaymentService(IRepository<Payment, String>  paymentRepository) {
        this.payRepo = paymentRepository;
    }

    List<Payment> getPayments() throws IOException {
        return payRepo.getAll();
    }

    public void addPayment(int table, PaymentType type, double amount) throws ServerException, ValidationException {
        // Validate table
        if(table < 0 || table > 7)
            throw new ValidationException("Parameter error", "Invalid table");

        // Validate amount
        if(amount <= 0)
            throw new ValidationException("Parameter error", "Invalid amount");

        Payment payment = new Payment(table, type, amount);
        try {
            payRepo.add(payment);
        } catch (IOException e) {
            throw new ServerException(e.getMessage());
        }
    }

    public double getTotalAmount(PaymentType type) throws IOException {
        double total = 0.0f;
        List<Payment> l = getPayments();
        if (l == null)
            return total;
        else if(l.isEmpty())
            return total;
        for (Payment p : l)
        {
            if (p.getType().equals(type))
                total += p.getAmount();
        }
        return total;
    }

    public Double getPaymentsTotalAmount(List<Order> menuData) {
        return menuData.stream()
                .filter(x -> x.getQuantity() > 0)
                .map(order -> order.getQuantity() * order.getMenuItem().getPrice())
                .collect(Collectors.toList())
                .stream()
                .mapToDouble(Double::doubleValue)
                .sum();
    }

}
