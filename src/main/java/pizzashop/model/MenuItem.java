package pizzashop.model;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

public class MenuItem {

    private final SimpleStringProperty name;
    private final SimpleDoubleProperty price;

    public MenuItem(String mItem, Double mPrice) {
        this.name = new SimpleStringProperty(mItem);
        this.price = new SimpleDoubleProperty(mPrice);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public Double getPrice() {
        return price.get();
    }

    public SimpleDoubleProperty priceProperty() {
        return price;
    }

    public void setPrice(Double price) {
        this.price.set(price);
    }
}
