package pizzashop.model.enums;

public enum PaymentType {
    CASH, CARD
}
