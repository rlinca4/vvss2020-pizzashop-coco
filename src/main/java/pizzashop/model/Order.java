package pizzashop.model;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

public class Order {

    private MenuItem menuItem;
    private Integer quantity;

    public Order(MenuItem menuItem, Integer quantity) {
        this.menuItem = menuItem;
        this.quantity = quantity;
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }

    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getMenuItemName() {
        return this.menuItem.getName();
    }

    public Double getMenuItemPrice() {
        return this.menuItem.getPrice();
    }

    public SimpleStringProperty getMenuItemNameProperty() {
        return this.menuItem.nameProperty();
    }

    public SimpleDoubleProperty getMenuItemPriceProperty() {
        return this.menuItem.priceProperty();
    }

    public void setMenuItemName(String menuItemName) {
        this.menuItem.setName(menuItemName);
    }

    public void setMenuItemPrice(Double menuItemPrice) {
        this.menuItem.setPrice(menuItemPrice);
    }
}
