package pizzashop.repository;

import java.io.IOException;
import java.util.List;

public interface IRepository<T, I> {
    void readData() throws IOException;
    void add(T t) throws IOException;
    void remove(T t) throws IOException;
    T getItem(I id);
    List<T> getAll() throws IOException;
}
