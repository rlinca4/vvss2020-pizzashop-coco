package pizzashop.repository;

import pizzashop.model.MenuItem;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class MenuRepository implements IRepository<MenuItem, String> {
    private static String filename = "data/menu.txt";
    private ArrayList listMenu;

    public MenuRepository() throws IOException {
        readData(); //create a new menu for each table, on request
    }

    public void readData() throws IOException {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        File file = new File(classloader.getResource(filename).getFile());
        this.listMenu = new ArrayList();
        try (BufferedReader br =
                     new BufferedReader(new FileReader(file))) {
            String line = null;
            while ((line = br.readLine()) != null) {
                MenuItem menuItem = getItem(line);
                listMenu.add(menuItem);
            }
        }
    }

    @Override
    public void add(MenuItem menuItem) {
        throw new NotImplementedException();
    }

    @Override
    public void remove(MenuItem menuItem) {
        throw new NotImplementedException();
    }

    public MenuItem getItem(String line) {
        MenuItem item = null;
        if (line == null || line.equals("")) return null;
        StringTokenizer st = new StringTokenizer(line, ",");
        String name = st.nextToken();
        double price = Double.parseDouble(st.nextToken());
        item = new MenuItem(name, price);
        return item;
    }

    public List<MenuItem> getAll() throws IOException {
        return listMenu;
    }

}
