package pizzashop.gui.components;

public interface PaymentOperation {
     void cardPayment();
     void cashPayment();
     void cancelPayment();
}
