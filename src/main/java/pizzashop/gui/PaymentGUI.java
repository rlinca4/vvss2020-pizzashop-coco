package pizzashop.gui;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import pizzashop.exception.BusinessException;
import pizzashop.exception.ServerException;
import pizzashop.exception.ValidationException;
import pizzashop.gui.components.PaymentOperation;
import pizzashop.model.enums.PaymentType;
import pizzashop.service.PaymentService;

import java.util.Optional;

public class PaymentGUI implements PaymentOperation {
    private PaymentService paymentService;

    public PaymentGUI(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @Override
    public void cardPayment() {
        System.out.println("--------------------------");
        System.out.println("Paying by card...");
        System.out.println("Please insert your card!");
        System.out.println("--------------------------");
    }

    @Override
    public void cashPayment() {
        System.out.println("--------------------------");
        System.out.println("Paying cash...");
        System.out.println("Please show the cash...!");
        System.out.println("--------------------------");
    }

    @Override
    public void cancelPayment() {
        System.out.println("--------------------------");
        System.out.println("Payment choice needed...");
        System.out.println("--------------------------");
    }

    public void showPaymentAlert(int tableNumber, double totalAmount) {
        try {
            Alert paymentAlert = new Alert(Alert.AlertType.CONFIRMATION);
            paymentAlert.setTitle("Payment for Table " + tableNumber);
            paymentAlert.setHeaderText("Total amount: " + totalAmount);
            paymentAlert.setContentText("Please choose payment option");
            ButtonType cardPayment = new ButtonType("Pay by CARD");
            ButtonType cashPayment = new ButtonType("Pay CASH");
            ButtonType cancel = new ButtonType("Cancel");
            paymentAlert.getButtonTypes().setAll(cardPayment, cashPayment, cancel);
            Optional<ButtonType> result = paymentAlert.showAndWait();

            if (result.get() == cardPayment) {
                cardPayment();
                paymentService.addPayment(tableNumber, PaymentType.CARD, totalAmount);
            } else if (result.get() == cashPayment) {
                cashPayment();
                paymentService.addPayment(tableNumber, PaymentType.CASH, totalAmount);
            } else if (result.get() == cancel) {
                cancelPayment();
            } else {
                cancelPayment();
            }
        } catch (ServerException | ValidationException e) {
            // V1. Show error on console
            e.printStackTrace();
        }
    }
}
