package pizzashop.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import pizzashop.exception.ServerException;
import pizzashop.model.MenuItem;
import pizzashop.gui.PaymentGUI;
import pizzashop.model.Order;
import pizzashop.service.PaymentService;
import pizzashop.service.PizzaService;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OrdersGUIController extends MainGUIController {

    private static final Logger logger = Logger.getLogger(OrdersGUIController.class.getName());

    @FXML
    private ComboBox<Integer> orderQuantity;
    @FXML
    private TableView orderTable;
    @FXML
    private TableColumn tableQuantity;
    @FXML
    protected TableColumn tableMenuItem;
    @FXML
    private TableColumn tablePrice;
    @FXML
    private Label pizzaTypeLabel;
    @FXML
    private Button addToOrder;
    @FXML
    private Label orderStatus;
    @FXML
    private Button placeOrder;
    @FXML
    private Button orderServed;
    @FXML
    private Button payOrder;
    @FXML
    private Button newOrder;

    private List<String> orderList = FXCollections.observableArrayList();

    public static double getTotalAmount() {
        return totalAmount;
    }

    public static void setTotalAmount(double totalAmount) {
        OrdersGUIController.totalAmount = totalAmount;
    }

    private int tableNumber;

    private TableView<MenuItem> table = new TableView<>();
    private Calendar calendar = Calendar.getInstance();
    private static double totalAmount;

    public void setService(PizzaService pizzaService, PaymentService paymentService, int tableNumber) {
        this.pizzaService = pizzaService;
        this.paymentService = paymentService;

        this.tableNumber = tableNumber;
        initData();
    }

    private void initData() {
        try {
            ObservableList<Order> menuData = FXCollections.observableArrayList(pizzaService.getPossibleOrders());
            menuData.setAll(pizzaService.getPossibleOrders());
            orderTable.setItems(menuData);

            //Controller for Place Order Button
            placeOrder.setOnAction(event -> {
                orderList = pizzaService.getActualOrdersToString(menuData);
                KitchenGUIController.order.add("Table" + tableNumber + " " + orderList.toString());
                orderStatus.setText("Order placed at: " + calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE));
            });

            //Controller for Order Served Button
            orderServed.setOnAction(event -> orderStatus.setText("Served at: " + calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE)));

            //Controller for Pay Order Button
            payOrder.setOnAction(event -> {
                setTotalAmount(paymentService.getPaymentsTotalAmount(menuData));
                orderStatus.setText("Total amount: " + getTotalAmount());
                logger.info("--------------------------");
                logger.info("Table: " + tableNumber);
                logger.info("Total: " + getTotalAmount());
                logger.info("--------------------------");
                PaymentGUI pay = new PaymentGUI(paymentService);
                pay.showPaymentAlert(tableNumber, getTotalAmount());
            });
        } catch (ServerException e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
        }

    }

    @Override
    public void initialize() {

        //populate table view with menuData from OrderGUI
        table.setEditable(true);
        tableMenuItem.setCellValueFactory(
                new PropertyValueFactory<Order, String>("menuItemName"));
        tablePrice.setCellValueFactory(
                new PropertyValueFactory<Order, Double>("menuItemPrice"));
        tableQuantity.setCellValueFactory(
                new PropertyValueFactory<Order, Integer>("quantity"));

        //bind pizzaTypeLabel and quantity combo box with the selection on the table view
        orderTable.getSelectionModel().selectedItemProperty().addListener((ChangeListener<Order>) (observable, oldValue, newValue) -> pizzaTypeLabel.textProperty().bind(newValue.getMenuItemNameProperty()));

        //Populate Combo box for Quantity
        ObservableList<Integer> quantityValues = FXCollections.observableArrayList(0, 1, 2, 3, 4, 5);
        orderQuantity.getItems().addAll(quantityValues);
        orderQuantity.setPromptText("Quantity");

        //Controller for Add to order Button
        addToOrder.setOnAction(event -> orderTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Order>() {
            @Override
            public void changed(ObservableValue<? extends Order> observable, Order oldValue, Order newValue) {
                oldValue.setQuantity(orderQuantity.getValue());
                orderTable.getSelectionModel().selectedItemProperty().removeListener(this);
                table.refresh();
            }
        }));

        //Controller for Exit table Button
        newOrder.setOnAction(event -> {
            Alert exitAlert = new Alert(Alert.AlertType.CONFIRMATION, "Exit table?", ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> result = exitAlert.showAndWait();
            if (result.get() == ButtonType.YES) {
                Stage stage = (Stage) newOrder.getScene().getWindow();
                stage.close();
            }
        });
    }
}