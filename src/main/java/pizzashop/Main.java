package pizzashop;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import pizzashop.controller.MainGUIController;
import pizzashop.gui.KitchenGUI;
import pizzashop.model.enums.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PaymentService;
import pizzashop.service.PizzaService;

import java.io.IOException;
import java.util.Optional;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        MenuRepository repoMenu = new MenuRepository();
        PaymentRepository payRepo = new PaymentRepository();

        PizzaService pizzaService = new PizzaService(repoMenu);
        PaymentService paymentService = new PaymentService(payRepo);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/mainFXML.fxml"));
        Parent box = loader.load();
        MainGUIController ctrl = loader.getController();
        ctrl.setServices(pizzaService, paymentService);
        primaryStage.setTitle("PizeriaX");
        primaryStage.setResizable(false);
        primaryStage.setAlwaysOnTop(false);
        primaryStage.setOnCloseRequest(event -> {
            Alert exitAlert = new Alert(Alert.AlertType.CONFIRMATION, "Would you like to exit the Main window?", ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> result = exitAlert.showAndWait();
            if (result.get() == ButtonType.YES) {
                try {
                    System.out.println("Incasari cash: " + paymentService.getTotalAmount(PaymentType.CASH));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    System.out.println("Incasari card: " + paymentService.getTotalAmount(PaymentType.CARD));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                primaryStage.close();
            }
            // consume event
            else if (result.get() == ButtonType.NO) {
                event.consume();
            } else {
                event.consume();

            }

        });
        primaryStage.setScene(new Scene(box));
        primaryStage.show();
        KitchenGUI kitchenGUI = new KitchenGUI();
        kitchenGUI.KitchenGUI();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
